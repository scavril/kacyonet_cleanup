"""
 Kacyonet Cleanup
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json

import local_database
import kc_training
import kc_classe
import kc_trainee
import kc_trainer
import kc_connection

def main():
    """
    Main entry point 
    Used to call all other methods
    """
    # Open and read the configuration file
    with open('etc/config.json', 'r') as config_file:
        config_data = config_file.read()

    # config data as dict
    config_obj = json.loads(config_data)

    # instanciate objects to connect to local databases
    sf_db = local_database.LocalDatabase(config_obj['db_user'], config_obj['db_password'], config_obj['db_host'], config_obj['db_import'])
    kc_db = local_database.LocalDatabase(config_obj['db_user'], config_obj['db_password'], config_obj['db_host'], config_obj['db_export'])

    # Instanciate a api connection object
    api_connection = kc_connection.KacyonetApi(config_obj)

    # Instanciate trainings
    #training_obj = kc_training.KacyonetTrainings(sf_db, kc_db, api_connection)
    # Process trainings
    #training_obj.process_trainings()

    # Instanciate classes
    class_obj = kc_classe.KacyonetClasse(sf_db, kc_db, api_connection)
    # Process classes
    class_obj.process_classes()

    # Instanciate trainee
    #trainee_obj = kc_trainee.KacyonetTrainee(sf_db, kc_db, api_connection)
    # Process classes
    #trainee_obj.process_trainee()

    # Instanciate trainer
    #trainee_obj = kc_trainer.KacyonetTrainer(sf_db, kc_db, api_connection)
    # Process classes
    #trainee_obj.process_trainer()

if __name__ == '__main__':
    main()
