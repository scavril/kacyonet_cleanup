"""
 Kacyonet base class
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import mysql.connector
import datetime

import kc_connection
import utils

class KacyonetTableDescriptor():
    """
    Class to hold a db table descriptor
    """

    def __init__(self, table_name, table_columns):
        """
        Init table names and values
        """
        self.table_name = table_name
        self.table_columns = table_columns

class KacyonetBase():
    """
    Class to manage call and cleanup of Trainings in Kacyonet
    """

    def __init__(self, sf_db, kc_db, a_api):
        """
        Init : 
        - sf_db is a database object to the salesforce db
        - kc_db is a database object to the kacyonet db (result)
        - a_api is a KacyonetApi object
        """
        self._sf_db = sf_db
        self._kc_db = kc_db
        self._api = a_api

    def _create_table(self, table_desc):
        """
        Create a table based on a table descriptor (see class above)
        """
        drop_table = 'DROP TABLE IF EXISTS ' + table_desc.table_name
        self._kc_db.write_query(drop_table)
        create_query = 'CREATE TABLE class_outdated ('
        first_col = True
        for a_column in table_desc.table_columns:
            if first_col:
                create_query = create_query + a_column + ' VARCHAR(32) DEFAULT NULL,'
                first_col = False
            else:
                create_query = create_query + a_column + ' TEXT DEFAULT NULL,'
        create_query = create_query + 'PRIMARY KEY(' + table_desc.table_columns[0] + '))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4'
        self._kc_db.write_query(create_query)

    def _db_insert(self, table_desc, values_array, selector_array):
        """
        Insert values in a table
        - based on a table descriptor (see class above)
        - an array of values (with selected indices - selector_array)
        """
        
        # Format a row value expression
        row_values = ''
        for a_selector in selector_array:
            a_value = values_array[a_selector]
            if not isinstance(a_value, str):
                a_value = str(a_value)
            row_values = row_values + '"' + a_value + '",'
        row_values = row_values[:-1]

        column_ids= ""
        for a_column in table_desc.table_columns:
            column_ids = column_ids + a_column + ','
        column_ids = column_ids[:-1]

        insert_query = 'INSERT INTO ' + table_desc.table_name + ' (' + column_ids + ') VALUES (' + row_values + ')'
        self._kc_db.write_query(insert_query)
        


