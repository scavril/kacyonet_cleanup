"""
 Kacyonet API extraction of Trainer
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import mysql.connector
import urllib

import kc_connection

class KacyonetTrainer():
    """
    Class to manage call and cleanup of Trainers in Kacyonet
    """

    def __init__(self, sf_db, kc_db, a_api):
        """
        Init : 
        - sf_db is a database object to the salesforce db
        - kc_db is a database object to the kacyonet db (result)
        - a_api is a KacyonetApi object
        """
        self._sf_db = sf_db
        self._kc_db = kc_db
        self._api = a_api

    def process_trainer(self):
        # extract enroll id
        self.get_kacyonet_trainer_enroll_ids()

    def get_kacyonet_trainer_enroll_ids(self):
        """
        Build the trainer enroll ids of Kacyonet TraineeEnrolls
        Base on the ClasseSteps listed in classstep_to_delete table
        """
        # First create the classstep_to_delete
        drop_table = 'DROP TABLE IF EXISTS trainer_enroll_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE trainer_enroll_to_delete (step_id VARCHAR(32) DEFAULT NULL, trainer_id TEXT DEFAULT NULL, sf_id TEXT DEFAULT NULL, PRIMARY KEY(step_id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        a_query = 'SELECT id FROM classstep_to_delete'
        query_result = self._kc_db.read_query(a_query)
        class_step_ids = []
        for a_result in query_result:
            class_step_ids.append(a_result[0])

        # Now read all trainee enrolls
        # Note: we read all trainee enrolls as the call to
        # TraineeEnrolls with key/value does not work
        a_end_point = 'TrainerEnrolls'
        api_response = self._api.get_api(a_end_point)
        for a_response in api_response:
            try:
                a_step_id = str(a_response['STEP_ID'])
                x = class_step_ids.index(a_step_id)
                # training_id being in the array to delete
                # add the training step to the database
                a_trainer_id = str(a_response['TRAINER_ID'])
                a_ext_id = a_response['EXTID']
                if a_ext_id is None:
                    a_ext_id = 'None'
                row_values = '"' + a_step_id + '","' + a_trainer_id + '","' + a_ext_id + '"'
                sql_query = 'INSERT INTO trainer_enroll_to_delete (step_id, trainer_id, sf_id) VALUES (' + row_values + ')'
                self._kc_db.write_query(sql_query)
                print('Storing trainer enroll ids for trainer ' + a_trainer_id + ' for step id ' + a_step_id)
            except:
                pass



 