"""
 Kacyonet API extraction of Classes
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import mysql.connector
import datetime

from kc_base import KacyonetBase, KacyonetTableDescriptor
import kc_connection
import utils

class KacyonetClasse(KacyonetBase):
    """
    Class to manage call and cleanup of Trainings in Kacyonet
    """

    def __init__(self, sf_db, kc_db, a_api):
        KacyonetBase.__init__(self, sf_db, kc_db, a_api)
        
        self._class_outdated = KacyonetTableDescriptor(
            "class_outdated",
            [
                'id', 
                'id_training', 
                'name', 
                'sf_id'
            ]
        )

    def process_classes(self):
        # extract classes ids
        #self.get_kacyonet_classes_ids()
        # extract class steps id
        #self.get_kacyonet_classsteps_ids()
        # extract outdated class steps
        #self.get_kacyonet_outdated_steps()
        self.get_kacyonet_outdated_classes()

    def get_kacyonet_classes_ids(self):
        """
        Build the classes ids of Kacyonet Classes
        Base on the Training listed in training_to_delete table
        """
        # First create the classes_to_delete
        drop_table = 'DROP TABLE IF EXISTS classes_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE classes_to_delete (id VARCHAR(32) DEFAULT NULL, id_training TEXT DEFAULT NULL, name TEXT DEFAULT NULL, sf_id TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        a_query = 'SELECT id FROM training_to_delete'
        query_result = self._kc_db.read_query(a_query)
        training_ids = []
        for a_result in query_result:
            training_ids.append(a_result[0])

        # Now read all classes
        # Note: we read all classes as the call to
        # SessionClasses with key/value does not work
        a_end_point = 'SessionClasses'
        api_response = self._api.get_api(a_end_point)
        for a_response in api_response:
            try:
                a_training_id = str(a_response['ELEM_ID'])
                x = training_ids.index(a_training_id)
                # training_id being in the array to delete
                # add the training step to the database
                a_class_id = str(a_response['CLASS_ID'])
                a_name = a_response['NAME']
                a_sf_id = a_response['EXTID']
                row_values = '"' + a_class_id + '","' + a_training_id + '","' + a_name + '","' + a_sf_id + '"'
                sql_query = 'INSERT INTO classes_to_delete (id, id_training, name, sf_id) VALUES (' + row_values + ')'
                self._kc_db.write_query(sql_query)
                print('Storing classes id ' + a_class_id + ' for training id ' + a_training_id)
            except:
                pass


    def get_kacyonet_classsteps_ids(self):
        """
        Build the class step ids of Kacyonet that need to be deleted
        Based on the training ids in classes_to_delete table
        """
        # First create the training_to_delete_table
        drop_table = 'DROP TABLE IF EXISTS classstep_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE classstep_to_delete (id VARCHAR(32) DEFAULT NULL, id_training TEXT DEFAULT NULL, id_class TEXT DEFAULT NULL, name TEXT DEFAULT NULL, sf_id TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        # Then read the trainings ids to be deleted
        a_query = 'SELECT id FROM training_to_delete'
        query_result = self._kc_db.read_query(a_query)
        a_count = len(query_result)
        training_ids = []
        for a_result in query_result:
            training_ids.append(a_result[0])
        
        # Now read all class steps
        # Note: we read all class steps as the call to
        # SessionClassSteps with key/value does not work
        a_end_point = 'SessionClassSteps'
        api_response = self._api.get_api(a_end_point)
        for a_response in api_response:
            try:
                a_training_id = str(a_response['TRAINING_ID'])
                x = training_ids.index(a_training_id)
                # training_id being in the array to delete
                # add the training step to the database
                a_step_id = str(a_response['STEP_ID'])
                a_class_id = str(a_response['CLASS_ID'])
                a_name = a_response['NAME']
                a_sf_id = a_response['EXTID']
                row_values = '"' + a_step_id + '","' + a_training_id+ '","' + a_class_id + '","' + a_name + '","' + a_sf_id + '"'
                sql_query = 'INSERT INTO classstep_to_delete (id, id_training, id_class, name, sf_id) VALUES (' + row_values + ')'
                self._kc_db.write_query(sql_query)
                print('Storing class step id ' + a_step_id + ' for training id ' + a_training_id)
            except:
                pass

    def get_kacyonet_outdated_steps(self):
        """
        Build the class step ids of Kacyonet that need to be deleted
        Based on the end date < 2020-10-01
        """
        # First create the training_to_delete_table
        drop_table = 'DROP TABLE IF EXISTS classstep_outdated'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE classstep_outdated (id VARCHAR(32) DEFAULT NULL, id_training TEXT DEFAULT NULL, id_class TEXT DEFAULT NULL, name TEXT DEFAULT NULL, end_date TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        # Get class step already to be deleted
        a_query = 'SELECT id FROM classstep_to_delete'
        query_result = self._kc_db.read_query(a_query)
        deleted_steps = []
        for a_result in query_result:
            deleted_steps.append(a_result[0])

        # Now read all class steps
        # Note: we read all class steps as the call to
        # SessionClassSteps with key/value does not work
        a_end_point = 'SessionClassSteps'
        api_response = self._api.get_api(a_end_point)
        for a_response in api_response:
            a_step_id = str(a_response['STEP_ID'])
            try:
                x = deleted_steps.index(a_step_id)
            except:
                # Step not already in the steps to delete
                a_end_date = a_response['DATE_END']
                if a_end_date is None:
                    a_end_date = '1970-01-01'
                if utils.invalid_step_date(a_end_date):
                    a_training_id = str(a_response['TRAINING_ID'])
                    a_class_id = str(a_response['CLASS_ID'])
                    a_name = utils.cleanup_string(a_response['NAME'])
                    row_values = '"' + a_step_id + '","' + a_training_id+ '","' + a_class_id + '","' + a_name + '","' + a_end_date + '"'
                    sql_query = 'INSERT INTO classstep_outdated (id, id_training, id_class, name, end_date) VALUES (' + row_values + ')'
                    self._kc_db.write_query(sql_query)
                    print('Storing outdated class step id ' + a_step_id + ' end date ' + a_end_date)

    def get_kacyonet_outdated_classes(self):
        """
        Build the class ids of Kacyonet that need to be deleted
        Based on class steps outdated
        """
        # First create the training_to_delete_table
        self._create_table(self._class_outdated)

        # Get class step already to be deleted
        a_query = 'SELECT DISTINCT(id_class) FROM classstep_outdated'
        query_result = self._kc_db.read_query(a_query)
        deleted_classes = []
        for a_result in query_result:
            deleted_classes.append(a_result[0])

        # Now read all classes
        # Note: we read all classes as the call to
        # SessionClasses with key/value does not work
        api_response = self._api.get_api('SessionClasses')
        for a_response in api_response:
            try:
                x = deleted_classes.index(str(a_response['CLASS_ID']))
                self._db_insert(self._class_outdated, 
                    a_response, 
                    ['CLASS_ID', 'ELEM_ID', 'NAME', 'EXTID']
                )
            except:
                pass

 