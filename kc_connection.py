"""
 Kacyonet API utilities
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import requests

kc_connected = False
kc_url = ''

class KacyonetApi():
    """
    Class to encapsulate the Kacyonet API calls
    """

    def __init__(self, a_configuration):
        """
        Create a connection to the API
        and get a token for subsequent calls
        """
        self._token = ""
        self._configuration = a_configuration
        a_response = self._authenticate()

    def _authenticate(self):
        kc_url = self._configuration['kacyonet_api']
        authenticate_obj = {
            "Login": self._configuration['kacyonet_login'],
            "Password": self._configuration['kacyonet_password']
        }
        a_response = self.post_api('Authenticate', authenticate_obj)
        # Analyse response
        if a_response['Status'] == 200:
            # if ok get access token for subsequent calls
            a_result = a_response['Response']
            a_token = a_result['access_token']
            # store the token in the headers
            self._token = a_token
            return True
        return False

    def post_api(self, end_point, input_data):
        """
        Request the API endpoint with the given input_data
        input_data is a dict
        """
        kacyonet_url = self._configuration['kacyonet_api'] + '/' + end_point
        
        data_str = json.dumps(input_data)
        content_length = len(data_str)
        a_headers = {
            'Content-Type': 'application/json',
            'Content-Length': str(content_length),
            'Host': 'first-finance.impulsesignin.com',
            'Accept': 'application/json',
        }

        a_response = requests.post(kacyonet_url, data=data_str, headers=a_headers)
        json_response = json.loads(a_response.text)
        return json_response

    def get_api(self, end_point):
        """
        Request the API endpoint with the given input_data
        input_data is a dict
        """
        kacyonet_url = self._configuration['kacyonet_api'] + '/' + end_point      
        a_headers = {
            'Host': 'first-finance.impulsesignin.com',
            'Authorization': self._token,
        }
        a_response = requests.get(kacyonet_url, headers=a_headers)
        if a_response.status_code == 200:
            json_response = json.loads(a_response.text)
            return json_response
        return ''




