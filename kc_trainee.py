"""
 Kacyonet API extraction of Trainees
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import mysql.connector
import urllib

import kc_connection

class KacyonetTrainee():
    """
    Class to manage call and cleanup of Trainees in Kacyonet
    """

    def __init__(self, sf_db, kc_db, a_api):
        """
        Init : 
        - sf_db is a database object to the salesforce db
        - kc_db is a database object to the kacyonet db (result)
        - a_api is a KacyonetApi object
        """
        self._sf_db = sf_db
        self._kc_db = kc_db
        self._api = a_api

    def process_trainee(self):
        # extract enroll id
        self.get_kacyonet_trainee_enroll_ids()

    def get_kacyonet_trainee_enroll_ids(self):
        """
        Build the trainee enroll ids of Kacyonet TraineeEnrolls
        Base on the Classes listed in classes_to_delete table
        """
        # First create the classes_to_delete
        drop_table = 'DROP TABLE IF EXISTS trainee_enroll_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE trainee_enroll_to_delete (id VARCHAR(32) DEFAULT NULL, user_id TEXT DEFAULT NULL, class_id TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        a_query = 'SELECT id FROM classes_to_delete'
        query_result = self._kc_db.read_query(a_query)
        class_ids = []
        for a_result in query_result:
            class_ids.append(a_result[0])

        # Now read all trainee enrolls
        # Note: we read all trainee enrolls as the call to
        # TraineeEnrolls with key/value does not work
        a_end_point = 'TraineeEnrolls'
        api_response = self._api.get_api(a_end_point)
        for a_response in api_response:
            try:
                a_class_id = str(a_response['CLASS_ID'])
                x = class_ids.index(a_class_id)
                # training_id being in the array to delete
                # add the training step to the database
                a_enroll_id = str(a_response['ENROLL_ID'])
                a_user_id = str(a_response['USER_ID'])
                row_values = '"' + a_enroll_id + '","' + a_user_id + '","' + a_class_id + '"'
                sql_query = 'INSERT INTO trainee_enroll_to_delete (id, user_id, class_id) VALUES (' + row_values + ')'
                self._kc_db.write_query(sql_query)
                print('Storing user enroll ids for user ' + a_user_id + ' for class id ' + a_class_id)
            except:
                pass



 