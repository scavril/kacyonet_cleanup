"""
 Utilities
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import datetime

replacements_chars = [
    ['"', ""],
    ['\\', ""],
    ['\00', " "]
]

reference_date = datetime.datetime.strptime('2020-10-01', '%Y-%m-%d')

def cleanup_string(a_string):
    """
    Remove special charaters in a string
    to be able to record it in database
    """
    for a_replacement in replacements_chars:
        text_to_search = a_replacement[0]
        replacement_text = a_replacement[1]
        # Search and replace some strange characters
        a_string = a_string.replace(text_to_search, replacement_text)

    return a_string

def invalid_step_date(a_date_string):
    """
    Check if a given data if less than reference date
    """
    a_date = datetime.datetime.strptime(a_date_string[:10], '%Y-%m-%d')
    return a_date < reference_date

