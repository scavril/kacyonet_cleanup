"""
 Local database object connections
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import mysql.connector

class LocalDatabase():
    """
    Class to connect to database
    """

    def __init__(self, db_user, db_password, db_host, db_name):
        """
        Init : a_configuration is a configuraiton dict
        """
        self._connection = mysql.connector.connect(user=db_user, password=db_password, host=db_host, database=db_name)
        self._cursor = self._connection.cursor()

    def read_query(self, a_query):
        self._cursor.execute(a_query)
        query_result = self._cursor.fetchall()
        return query_result

    def write_query(self, a_query):
        self._cursor.execute(a_query)
        self._connection.commit()

