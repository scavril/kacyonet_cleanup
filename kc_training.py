"""
 Kacyonet API extraction of Trainings
 March 2021, Copyright First Finance
"""
__author__ = "Stephane Cavril"
__copyright__ = "Copyright 2021, First Finance"
__email__ = "sc@freedom-partners.com"

import json
import mysql.connector
import urllib

import kc_connection
import utils

class KacyonetTrainings():
    """
    Class to manage call and cleanup of Trainings in Kacyonet
    """

    def __init__(self, sf_db, kc_db, a_api):
        """
        Init : 
        - sf_db is a database object to the salesforce db
        - kc_db is a database object to the kacyonet db (result)
        - a_api is a KacyonetApi object
        """
        self._sf_db = sf_db
        self._kc_db = kc_db
        self._api = a_api
        self._replacements = [
            ['"', ""],
            ['\\', ""],
            ['\00', " "]
        ]


    def process_trainings(self):
        """
        Read the Salesforce product and store the MOOC ones
        """
        # First create the table to hold product ids to delete
        drop_table = 'DROP TABLE IF EXISTS product_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE product_to_delete (id VARCHAR(32) DEFAULT NULL, name TEXT DEFAULT NULL, title TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        # Select MOOC Products
        self._select_mooc()

        # Then extract training ids in Kacyonet from this list
        self.get_kacyonet_training_ids()
        # Then extract training steps ids
        self.get_kacyonet_trainingsteps_ids()

    def _select_mooc(self):
        """
        Read the Salesforce db and select products that are MOOC
        """
        final_product_array = []
        a_query = 'SELECT Id, Name, Titre_du_produit__c FROM Produit__c WHERE MOOC__c="1"'
        query_result = self._sf_db.read_query(a_query)
        for a_result in query_result:
            a_final_product = {
                "id": a_result[0],
                "name": a_result[1],
                "title": a_result[2]
            }
            final_product_array.append(a_final_product)

        # Store the result in the product_to_delete table
        for a_product in final_product_array:
            row_values = '"' + a_product["id"] + '","' + a_product["name"] + '","' + a_product["title"] + '"'
            sql_query = 'INSERT INTO product_to_delete (id, name, title) VALUES (' + row_values + ')'
            self._kc_db.write_query(sql_query)

    def get_kacyonet_training_ids(self):
        """
        Build the training ids of Kacyonet Trainings
        Base on the Products listed in product_to_delete table
        """
        # First create the training_to_delete_table
        drop_table = 'DROP TABLE IF EXISTS training_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE training_to_delete (id VARCHAR(32) DEFAULT NULL, name TEXT DEFAULT NULL, sf_id TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        # Then read the product ids to be deleted
        index = 0
        a_query = 'SELECT id FROM product_to_delete'
        query_result = self._kc_db.read_query(a_query)
        a_count = len(query_result)
        for a_result in query_result:
            # For each id, request the Kacyonet API to get the TRAINING id
            a_end_point = 'SessionTrainings/EXT_ID/' + a_result[0]
            api_response = self._api.get_api(a_end_point)
            if api_response != '':
                index = index + 1
                # Cleanup name
                a_name = utils.cleanup_string(api_response['NAME'])

                # if store result in training_to_delete table
                row_values = '"' + str(api_response["TRAINING_ID"]) + '","' + a_name + '","' + api_response["EXT_ID"] + '"'
                sql_query = 'INSERT INTO training_to_delete (id, name, sf_id) VALUES (' + row_values + ')'
                self._kc_db.write_query(sql_query)

                print('Storing training id ' + str(index) + ' / ' + str(a_count))

    def get_kacyonet_trainingsteps_ids(self):
        """
        Build the training step ids of Kacyonet that need to be deleted
        Based on the Training ids in training_to_delete table
        """
        # First create the training_to_delete_table
        drop_table = 'DROP TABLE IF EXISTS trainingstep_to_delete'
        self._kc_db.write_query(drop_table)
        create_table = "CREATE TABLE trainingstep_to_delete (id VARCHAR(32) DEFAULT NULL, id_training TEXT DEFAULT NULL, name TEXT DEFAULT NULL, sf_id TEXT DEFAULT NULL, PRIMARY KEY(id))  ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4"
        self._kc_db.write_query(create_table)

        # Then read the trainings ids to be deleted
        index = 0
        a_query = 'SELECT id FROM training_to_delete'
        query_result = self._kc_db.read_query(a_query)
        a_count = len(query_result)
        training_ids = []
        for a_result in query_result:
            training_ids.append(a_result[0])
        
        # Now read all training steps
        # Note: we read all training steps as the call to
        # TrainingSteps with key/value does not work
        a_end_point = 'SessionTrainingSteps'
        api_response = self._api.get_api(a_end_point)
        for a_response in api_response:
            try:
                a_training_id = str(a_response['TRAINING_ID'])
                x = training_ids.index(a_training_id)
                # training_id being in the array to delete
                # add the training step to the database
                a_step_id = str(a_response['STEP_ID'])
                a_name = a_response['NAME']
                a_sf_id = a_response['EXT_ID']
                row_values = '"' + a_step_id + '","' + a_training_id + '","' + a_name + '","' + a_sf_id + '"'
                sql_query = 'INSERT INTO trainingstep_to_delete (id, id_training, name, sf_id) VALUES (' + row_values + ')'
                self._kc_db.write_query(sql_query)
                print('Storing training step id ' + a_step_id + ' for training id ' + a_training_id)
            except:
                pass

if __name__ == '__main__':
    main()
    
 